(define-module (json-writer)
  #:use-module (json)
  #:use-module (ice-9 optargs)
  #:export (put-json-to-file))


(define* (put-json-to-file file-path the-json
                           #:key
                           (escape-slashes #f)
                           (pretty #f)
                           (escape-unicode #f))
  (scm->json the-json #:escape escape-slashes #:pretty pretty #:unicode escape-unicode))
