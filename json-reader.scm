(define-module (json-reader)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-43)
  #:use-module (rnrs bytevectors)
  #:export (scm->json
            scm->json-string))

(use-modules
 (ice-9 optargs)  ; for keyword arguments
 (json))


(define*-public (print-json the-json)
  (display (simple-format #f "~a\n" (scm->json-string the-json #:escape #f #:pretty #t))))


(define-public (get-json-from-file file-path)
  (call-with-input-file file-path
    (λ (port)
      (set-port-encoding! port "UTF-8")
      (json->scm port))))
