(add-to-load-path (dirname (current-filename)))

(use-modules
 (json)
 (json-reader)
 (json-writer)
 (srfi srfi-1)  ;; union and other set operations
 (srfi srfi-9)  ;; structs
 (srfi srfi-13)  ;; hash table procedures
 (srfi srfi-9 gnu)  ;; for functional structs (not part of srfi-9 directly)
 (srfi srfi-37)  ;; command line arguments
 (ice-9 hash-table))

;; ==============================
;; COMMAND LINE ARGUMENT HANDLING
;; ==============================

;; store the options globally
(define options (make-hash-table 10))

;; make a procedure that only displays message and then exits
(define (display-and-exit-proc msg)
  ;; needs to take the mandatory arguments though
  (lambda (opt name arg loads)
    ;; now only display
    (display msg)
    ;; and then quit the program
    (quit)))

(define* (make-store-in-options-proc #:optional (key #f))
  "Make a processor, which stores the option in the options hash
table, optionally taking a key under which to store the value."
  (lambda (opt name arg loads)
    (display
     (simple-format #f
                    "storing the following option and value: ~a, ~a\n"
                    (if key key name)
                    arg))
    (if key
        (hash-set! options key arg)
        (hash-set! options name arg))
    ;; "and the processor should return seeds as well."
    loads))

(define usage-help
  (string-join '(""
                 "main.scm [options]"
                 "-v,  --vocabulary    specify vocabulary file"
                 "-p,  --plot          specify progress plotting file"
                 "-h,  --help          display this help"
                 "")
               "\n"))

(define (is-regular-file? file-path)
  ;; https://www.gnu.org/software/guile/manual/html_node/File-System.html
  (call-with-input-file file-path
    (λ (port)
      (eq? (stat:type (stat port)) 'regular))))

(define option-spec
  ;; args-fold calls the processors of the options with the following arguments:
  ;; - the containing option object,
  ;; - the name used on the command line,
  ;; - the argument given for the option (or #f if none)
  ;; - the rest of the arguments are args-fold “seeds”
  ;;   and the processor should return seeds as well.
  ;; specify the options in a list of option objects
  (list (option '(#\v "vocabulary")  ; short name and long name
                #f  ; required-arg? - option must be followed by an argument
                #t  ; optional-arg? - option takes an argument if available
                ;; processor of option
                (λ (opt name arg loads)
                  (display (simple-format #f "opt: ~a\n" opt))
                  (display (simple-format #f "name: ~a\n" name))
                  (display (simple-format #f "arg: ~a\n" arg))
                  (display (simple-format #f "loads: ~a\n" loads))
                  (cond
                   [(not (file-exists? arg))
                    (error (simple-format #f "vocabulary file does not exist: ~a\n" arg))]
                   [(not (is-regular-file? arg))
                    (error (simple-format #f "vocabulary file is not a regular file: ~a\n" arg))]
                   [else
                    ((make-store-in-options-proc "vocabulary") opt name arg loads)])))
        (option '(#\p "plot") #t #f
                (λ (opt name arg loads)
                  (cond
                   [(not (file-exists? arg))
                    (error (simple-format #f "plot file does not exist: ~a\n" arg))]
                   [(not (is-regular-file? arg))
                    (error (simple-format #f "plot file is not a regular file: ~a\n" arg))]
                   [else
                    ((make-store-in-options-proc "plot") opt name arg loads)])))
        (option '(#\h "help") #f #f
                (display-and-exit-proc usage-help))))


(args-fold
 ;; (program-arguments) simply contains all arguments to the guile command
 ;; We do not need the filename of the program, so we discard it and only use the cdr.
 (cdr (program-arguments))
 ;; use previously defined option specification
 option-spec
 ;; What happens when unknown arguments are given?
 ;; Unknown argument handling procedure.
 (lambda (opt name arg loads)
   (error (simple-format #f "Unrecognized option: ~A\n~A" name usage-help)))
 ;; Call operand-proc with any items on the command line that are not named options.
 ;; This includes arguments after ‘--’.
 ;; It is called with the argument in question, as well as the seeds.
 (lambda (op loads)
   (cons op loads))
 ;; seed - What is the seed???
 '())

;; ===========================
;; VOCABULARY DATA ABSTRACTION
;; ===========================
(define (alist-ref alist ref)
  (println (simple-format #f "trying to get attribute ~s from ~s" ref alist))
  (cond [(null? alist) (throw 'attribute-not-found ref)]
        [(eq? (caar alist) ref) (cdar alist)]
        [else (alist-ref (cdr alist) ref)]))

(define (get-voc-item-meta voc-item)
  (alist-ref voc-item "metadata"))

(define (get-voc-item-translations voc-item)
  (println (simple-format #f "getting translations from ~s" voc-item))
  (alist-ref voc-item "translation_data"))

(define (get-voc-item-meta-attr voc-item getter)
  (let ([meta (get-voc-item-meta voc-item)])
    (getter meta)))

(define (get-voc-item-attr voc-item getter)
  (let ([translations (get-voc-item-translations voc-item)])
    (getter translations)))

(define (get-vocabulary-words vocabulary)
  #;(hash-ref vocabulary "words")
  (cond [(null? vocabulary) '()]
        [(string=? (caar vocabulary) "words") (cdar vocabulary)]
        [else (get-vocabulary-words (cdr vocabulary))]))

(define (make-voc-item-attr-getter attribute-name)
  (λ (voc-item)
    (alist-ref voc-item attribute-name)))

;; ===================
;; VOCABULARY HANDLING
;; ===================
(define (print-json the-json)
  (display (simple-format #f "~a\n" (scm->json-string the-json #:escape #f #:pretty #t))))

(define (println value)
  (display (simple-format #f "~s\n" value)))

(define (read-vocabulary file-path)
  (get-json-from-file file-path))

(define (save-vocabulary file-path vocabulary)
  (put-json-to-file file-path vocabulary))

(define (main options)
  ;; (println (hash-map->list cons options))
  ;; (println (read-vocabulary (hash-ref options "vocabulary" "default-vocabulary.json")))
  #;(println
   (hash-map->list cons (read-vocabulary (hash-ref options "vocabulary" "default-vocabulary.json"))))
  (let ([vocabulary (read-vocabulary (hash-ref options "vocabulary" "default-vocabulary.json"))])
    (println (get-voc-item-translations
              (vector-ref (get-vocabulary-words vocabulary) 0)))))


(main options)
